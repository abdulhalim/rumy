# -*- coding: utf-8 -*-
import urllib
import urllib2
import json
import requests
import datetime
import traceback

def delete():
    id = request.args(0,cast=int)
    #request.flash = id
    delete = db(db.Peripherals.id==id).delete()
    if delete:
        redirect(URL('index'))
    return dict()

def walkdict(d):
    stack = d.items()
    while stack:
        k, v = stack.pop()
        if isinstance(v, dict):
            stack.extend(v.iteritems())
        if k == 'device_id':
            return "%s" % v
        
        #else:
        #    return "%s: %s" % (k, v)

def walkdict_for_name(d):
    stack = d.items()
    while stack:
        k, v = stack.pop()
        if isinstance(v, dict):
            stack.extend(v.iteritems())
        if k == 'name':
            return "%s" % v

        
def walkdict_for_structure_id(d):
    stack = d.items()
    while stack:
        k, v = stack.pop()
        if k == 'structure_id':
            return "%s" % v
        else:
            if isinstance(v, dict):
                stack.extend(v.iteritems())

def walkdict_for_structure_name(d):
    stack = d.items()
    while stack:
        k, v = stack.pop()
        if k == 'name':
            return "%s" % v
        else:
            if isinstance(v, dict):
                stack.extend(v.iteritems())

                
def changes():
    return dict()

def index():
    if not auth.user_id:
        redirect('user/login')
    query = db.Peripherals.id == db.User_Peripheral_Detail.peripheral_id
    rows = db(query).select(orderby=auth.user_id)

    return dict(rows = rows)

def add_new():
    if not auth.user_id:
        redirect(URL('user/login'))
    return dict()

def authorization():
    device = request.vars['device_type']
    authorization_url = 'https://home.nest.com/login/oauth2?client_id=e2fdee25-2803-4965-b4f6-d300da5cac03&state=STATE'
    if device == 'nest':
        redirect(authorization_url)
    if device == "lifx":
        redirect(URL(get_lifx_key))    
    if device == 'rumy':
        if not (request.vars['name'] and request.vars['address']):
            redirect(URL(add_rumy_device))
        tempURL = "signature_update_rumy?device_type=%s" % device
        tempURL = tempURL + "&name=%s" % request.vars['name']
        tempURL = tempURL + "&address=%s" % request.vars['address']
        tempURL = tempURL + "&oAuth=%s" % request.vars['oAuth']
        redirect(URL(tempURL))
        return dict()
    return dict()

def nest_get_auth_from_token():
    state = request.vars['state']
    token = request.vars['code']
    url = "https://api.home.nest.com/oauth2/access_token"
    post_data = {}
    post_data['grant_type'] = "authorization_code"
    post_data['code'] = token
    post_data['client_id'] = "e2fdee25-2803-4965-b4f6-d300da5cac03"
    post_data['client_secret'] = "CUEDgheUAjViuaV8jGZMcDl98"
    #if request.application == "test_thermostat":
    #    post_data['client_id'] = "e2fdee25-2803-4965-b4f6-d300da5cac03"
    #    post_data['client_secret'] = "CUEDgheUAjViuaV8jGZMcDl98"
    #else:
    #    post_data['client_id'] = "b6b2cc18-456b-4397-8d79-8d100dea60fe"
    #    post_data['client_secret'] = "NhtlNiXcEQutts1TUS38nZhMg"

    post_data = urllib.urlencode(post_data)

    req = urllib2.Request(url, post_data)

    try:
        response = urllib2.urlopen(req)
    except Exception as e:
        error = e.read()
        print e
        print error
        try:
            return json.loads(error)
        except Exception as e:
            return dict(error=error)

    oauth = json.loads(response.read())['access_token']

    url = "https://developer-api.nest.com/devices.json"
    url += "?auth=" + oauth
    req = urllib2.Request(url)
    try:
        response = urllib2.urlopen(req)
    except Exception as e:
        print e
        try:
            error = e.read()
        except Exception:
            error = e
        print error
        try:
            return json.loads(error)
        except Exception:
            return dict(e=e, error=error)

    thermostats_json = json.loads(response.read())
    thermostat_id = thermostats_json['thermostats'][thermostats_json['thermostats'].keys()[0]]['device_id']

    redirect_url = "oAuth_update_peripheral"
    redirect_url += "?device_type=Nest"
    redirect_url += "&oAuth=" + oauth
    redirect_url += "&url=" + "https://developer-api.nest.com"
    redirect_url += "&device_id=" + thermostat_id
    redirect(redirect_url)

def get_lifx_key():
    form = FORM("Lifx API key:", INPUT(_name = "oauth"), INPUT(_type='submit'))
    if form.process().accepted:
        oauth = form.vars.oauth
        url = "add_lifx_device?oauth=" + oauth
        redirect(URL(url))

    return dict(form=form)

def add_lifx_device():
    oauth = request.vars['oauth']
    if not oauth:
        redirect(URL(get_lifx_key))

    r = requests.get('https://api.lifx.com/v1/lights/all', auth=(oauth, ''))
    if r.status_code != 200:
        session.flash = "Lifx request failed"
        redirect(URL(get_lifx_key))

    lifx_list = json.loads(r.content)

    dropdown_list = []
    for lifx in lifx_list:
        label = lifx['label']
        id = lifx['id']
#         dropdown_list.append(label + " " + id)
        dropdown_list.append(id)

    form = FORM(SELECT(dropdown_list, _name="lifx_id"), INPUT(_type='submit'))
    if form.process().accepted:
        lifx_id = form.vars.lifx_id
        redirect_url = "oAuth_update_peripheral"
        redirect_url += "?device_type=Lifx"
        redirect_url += "&oAuth=" + oauth
        redirect_url += "&url=" + "https://api.lifx.com/v1/lights/"
        redirect_url += "&device_id=" + lifx_id
        redirect(redirect_url)

    return dict(form=form)


def create_link_2():
    if not auth.user_id:
        redirect(URL('/user/login'))
    return dict(user_id=auth.user_id)

def add_rumy_device():
    return dict()

def nest_response_handler():
    return dict()

def get_body():
    return dict()

def oAuth_update_peripheral():
    device_type = request.vars['device_type']
    oAuth_code = request.vars['oAuth']
    url = request.vars['url']
    device_id = request.vars['device_id']

    form = SQLFORM(db.Peripherals)
    form = SQLFORM(db.Peripherals, fields=['peripheral_name', 'peripheral_type'])
    form.element('input', _type='submit')['_class'] = 'button'
    form.custom.submit['_id'] = 'submitButton'
    form.vars.oAuth=oAuth_code
    form.vars.peripheral_type=device_type
    form.vars.url = url
    form.vars.device_id = device_id
    if form.process().accepted:
        peripheral_id=form.vars.id
        userID=auth.user_id
        db.commit()
        db.User_Peripheral_Detail.insert(user_id = userID, peripheral_id = peripheral_id)
        db.commit()

        # tell nest script to connect to new thermostat
        #if request.application == "app_thomas2":
        #    url = "http://rumy.io:8081/api/connect_thermostat"
        #else:
        #    url = "http://rumy.io:8080/api/connect_thermostat"
        #put_data = {}
        #put_data['device_id'] = device_id
        #r = requests.put(url, json=put_data)
        #r.status_code, r.content

        #redirect(URL(create_link_2))
    return dict(form=form, oAuth_code=oAuth_code, peripheral_type = request.vars['device_type'])



def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()
