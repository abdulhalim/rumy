# -*- coding: utf-8 -*-
db.define_table('Peripherals',
                Field('peripheral_name', requires=IS_NOT_EMPTY()),
                Field('peripheral_type'),
                Field('oAuth'),
                Field('url'),
                Field('device_id'),
                Field('dial'))

db.define_table('User_Peripheral_Detail',
                Field('user_id', 'reference auth_user'),
                Field('peripheral_id', 'reference Peripherals'))
